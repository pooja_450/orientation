# GitLab :

GitLab is a single application for the entire DevOps lifecycle that allows teams to work together better and bring more value to your customers, faster.
GitLab does this by shortening your DevOps cycle time, bridging silos and stages, and taking work out of your hands. It also means a united workflow that reduces friction from traditionally separate activities like application security testing.

<img src="https://tse2.mm.bing.net/th?id=OIP.SCvEm2-f7NiqREPKN5u9aQHaIH&pid=Api&P=0&w=300&h=300" height=300px width=300px>

GitLab is a github like service that organizations can use to provide internal management of git repositories. It is a self hosted Git-repository management system that keeps the user code private and can easily deploy the changes of the code.

## Why use GitLab :
GitLab is great way to manage git repositories on centralized server. GitLab gives you complete control over your repositories or projects and allows you to decide whether they are public or private for free.

## Features of GitLab :
- **GitLab hosts your (private) software projects for free.**
- **GitLab is a platform for managing Git repositories.**
- **GitLab offers free public and private repositories, issue-tracking and wikis.**
- **GitLab is a user friendly web interface layer on top of Git, which increases the speed of working with Git.**
- **GitLab provides its own Continuous Integration (CI) system for managing the projects and provides user interface along with other features 
  of GitLab.**

## GitLab Workflow :

<img src="https://sdtimes.com/wp-content/uploads/2019/06/Screen-Shot-2019-06-20-at-4.25.04-PM-490x307.png" height=400px width=600px>
<img src="https://mlyrty6mr79m.i.optimole.com/doDcpiw-hESoWM38/w:auto/h:auto/q:auto/https://neoteric.eu/wp-content/uploads/2018/04/1.png">

## GitLab Advantages :

<img src="https://www.code-cafe.blog/wp-content/uploads/2018/02/Gitlab-Webinar.png" height=600px width=800px>

## GitLab CI :
GitLab CI/CD is a powerful tool built into GitLab that allows you to apply all the continuous methods (Continuous Integration, Delivery, and Deployment) to your software with no third-party application or integration needed.

## GitLab CD/CI Workflow :
<img src="https://docs.gitlab.com/ee/ci/introduction/img/gitlab_workflow_example_11_9.png">

Assume that you have discussed a code implementation in an issue and worked locally on your proposed changes. Once you push your commits to a feature branch in a remote repository in GitLab, the CI/CD pipeline set for your project is triggered. By doing so, GitLab CI/CD:

    
- Runs automated scripts (sequential or parallel) to:
        Build and test your app.
        Preview the changes per merge request with Review Apps, as you would see in your localhost. 

Once you’re happy with your implementation:

    
- Get your code reviewed and approved.
    
- Merge the feature branch into the default branch.
        GitLab CI/CD deploys your changes automatically to a production environment. 
And finally, you and your team can easily roll it back if something goes wrong. 

