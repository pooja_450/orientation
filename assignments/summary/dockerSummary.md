# Docker :
Docker is a software platform that allows you to build, test, and deploy applications quickly. Docker packages software into standardized units called containers that have everything the software needs to run including libraries, system tools, code, and runtime. Using Docker, you can quickly deploy and scale applications into any environment and know your code will run

<img src="https://www.shadowandy.net/wp/wp-content/uploads/docker.png" height=300px width=300px>

## How Docker Works :
Docker works by providing a standard way to run your code. Docker is an operating system for containers. Similar to how a virtual machine virtualizes (removes the need to directly manage) server hardware, containers virtualize the operating system of a server. Docker is installed on each server and provides simple commands you can use to build, start, or stop containers.

<img src="https://docs.microsoft.com/en-us/dotnet/architecture/microservices/docker-application-development-process/media/image1.png" height=500px width=700px>

## Example :
<img src="https://www.docker.com/sites/default/files/d8/styles/large/public/2018-11/Docker-Website-2018-Diagrams-071918-V5_x-windows-containers-page.png?itok=QKiUgfAL" height=500px width=700px>

## Features of Docker :
<img src="https://codecondo.com/wp-content/uploads/2018/07/Docker-use-cases.png" height=500px width=700px>

- Docker has the ability to reduce the size of development by providing a smaller footprint of the operating system via containers.
- With containers, it becomes easier for teams across different units, such as development, QA and Operations to work seamlessly across 
  applications
- You can deploy Docker containers anywhere, on any physical and virtual machines and even on the cloud.
- Since Docker containers are pretty lightweight, they are very easily scalable

## Components of Docker :
1. **Docker for Mac**
2. **Docker for windows**
3. **Docker for Linux**
4. **Docker Hub**
5. **Docker Engine**
6. **Docker Compose**

## Comparison between Virtualization and Docker :

- **Virtualization Architecture**
<img src="https://www.tutorialspoint.com/docker/images/virtualization.jpg" height=400px width=500px>

- **Docker Architecture**
<img src="https://www.tutorialspoint.com/docker/images/various_layers.jpg" height=400px width=500px>
