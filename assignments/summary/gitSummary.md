# GIT :
Git is a distributed version-control system for tracking changes in source code during software development. It is designed for coordinating work among programmers, but it can be used to track changes in any set of files. Its goals include speed, data integrity, and support for distributed, non-linear workflows

<img src="https://akamist.com/blog/wp-content/uploads/2017/08/icon_git.png" height=200px width=200px>

## Advantages of Git :
- **Free and open source**
- **Fast and small**
- **Implicit Backup**
- **Security**
- **No need of powerful hardware**
- **Easier branching**

## General idea of workflow :
- You clone git repository you want to work width
- You modify this copy by adding or editing the copy or you can even update it with other developer's updates
- You review the changes before commit
- You commit the changes and push them to the main repository 
- You can even edit your changes, correct your last commit and push them

<img src="https://d1jnx9ba8s6j9r.cloudfront.net/blog/wp-content/uploads/2016/11/Git-Architechture-Git-Tutorial-Edureka-2-768x720.png" height=500px width=500px>

## Precise working diagram :

<img src="https://www.tutorialspoint.com/git/images/life_cycle.png" height=700px width=550px>


